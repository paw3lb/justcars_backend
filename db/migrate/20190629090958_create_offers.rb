class CreateOffers < ActiveRecord::Migration[5.2]
  def change
    create_table :offers do |t|
      t.string :title
      t.text :description
      t.decimal :price, precision: 10, scale: 2
      t.integer :make_id, index: true
      t.integer :model_id, index: true

      t.timestamps
    end
  end
end
