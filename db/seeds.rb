# frozen_string_literal: true

# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: 'Star Wars' }, { name: 'Lord of the Rings' }])
#   Character.create(name: 'Luke', movie: movies.first)

audi = Make.find_or_create_by!(name: 'Audi')

a1 = Model.find_or_create_by!(name: 'A1', make_id: audi.id)
a2 = Model.find_or_create_by!(name: 'A2', make_id: audi.id)
a3 = Model.find_or_create_by!(name: 'A3', make_id: audi.id)
a4 = Model.find_or_create_by!(name: 'A4', make_id: audi.id)

[a1, a2, a3, a4].each do |model|
  car_options = Faker::Vehicle.car_options.join(', ')
  standard_specs = Faker::Vehicle.standard_specs.join(', ')
  offer = Offer.create!(
    title: "#{audi.name} #{model.name}",
    description: "Car options: #{car_options} Standard specs: #{standard_specs}",
    price: BigDecimal(rand(50_000...300_000)),
    make_id: audi.id,
    model_id: model.id
  )
  filename = "car#{rand(1..5)}.jpg"
  offer.image.attach(
    io: File.open(Rails.root.join('spec', 'support', 'assets', filename)),
    filename: filename,
    content_type: 'image/jpg'
  )
end

bmw = Make.find_or_create_by!(name: 'BMW')

x1 = Model.find_or_create_by!(name: 'X1', make_id: bmw.id)
x2 = Model.find_or_create_by!(name: 'X2', make_id: bmw.id)
x3 = Model.find_or_create_by!(name: 'X3', make_id: bmw.id)
x4 = Model.find_or_create_by!(name: 'X4', make_id: bmw.id)

[x1, x2, x3, x4].each do |model|
  car_options = Faker::Vehicle.car_options.join(', ')
  standard_specs = Faker::Vehicle.standard_specs.join(', ')
  offer = Offer.create!(
    title: "#{bmw.name} #{model.name}",
    description: "Car options: #{car_options} Standard specs: #{standard_specs}",
    price: BigDecimal(rand(50_000...300_000)),
    make_id: bmw.id,
    model_id: model.id
  )
  filename = "car#{rand(1..5)}.jpg"
  offer.image.attach(
    io: File.open(Rails.root.join('spec', 'support', 'assets', filename)),
    filename: filename,
    content_type: 'image/jpg'
  )
end

volvo = Make.find_or_create_by!(name: 'Volvo')

v40 = Model.find_or_create_by!(name: 'V40', make_id: volvo.id)
v50 = Model.find_or_create_by!(name: 'V50', make_id: volvo.id)
v60 = Model.find_or_create_by!(name: 'V60', make_id: volvo.id)
v70 = Model.find_or_create_by!(name: 'V70', make_id: volvo.id)

[v40, v50, v60, v70].each do |model|
  car_options = Faker::Vehicle.car_options.join(', ')
  standard_specs = Faker::Vehicle.standard_specs.join(', ')
  offer = Offer.create!(
    title: "#{volvo.name} #{model.name}",
    description: "Car options: #{car_options} Standard specs: #{standard_specs}",
    price: BigDecimal(rand(50_000...300_000)),
    make_id: volvo.id,
    model_id: model.id
  )
  filename = "car#{rand(1..5)}.jpg"
  offer.image.attach(
    io: File.open(Rails.root.join('spec', 'support', 'assets', filename)),
    filename: filename,
    content_type: 'image/jpg'
  )
end
