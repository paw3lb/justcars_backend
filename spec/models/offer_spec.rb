require 'rails_helper'

RSpec.describe Offer, type: :model do
  before(:all) do
    @offer = create(:offer)
  end

  it 'is valid with valid attributes' do
    expect(@offer).to be_valid
  end

  it 'is not valid without a title' do
    @offer.title = nil
    expect(@offer).to_not be_valid
  end

  it 'is not valid without a description' do
    @offer.description = nil
    expect(@offer).to_not be_valid
  end

  it 'is not valid without a price' do
    @offer.price = nil
    expect(@offer).to_not be_valid
  end

  it 'is not valid without a make' do
    @offer.make = nil
    expect(@offer).to_not be_valid
  end

  it 'is not valid without a model' do
    @offer.model = nil
    expect(@offer).to_not be_valid
  end

  it 'is not valid with too long title' do
    @offer.title = Faker::String.random(256)
    expect(@offer).to_not be_valid
  end

  it 'is not valid with too long description' do
    @offer.description = Faker::String.random(3_001)
    expect(@offer).to_not be_valid
  end

  it 'is not valid with too big price' do
    @offer.price = BigDecimal(100_000_001)
    expect(@offer).to_not be_valid
  end
end
