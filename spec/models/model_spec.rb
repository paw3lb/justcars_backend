require 'rails_helper'

RSpec.describe Model, type: :model do
  before(:each) do
    @model = create(:model)
  end

  it 'is valid with valid attributes' do
    expect(@model).to be_valid
  end

  it 'has a unique name' do
    model2 = build(:model, name: @model.name)
    expect(model2).to_not be_valid
  end

  it 'is not valid without a name' do
    @model.name = nil
    expect(@model).to_not be_valid
  end

  it 'is not valid without a make' do
    @model.make = nil
    expect(@model).to_not be_valid
  end
end
