require 'rails_helper'

RSpec.describe Make, type: :model do
  before(:each) do
    @make = create(:make)
  end

  it 'is valid with valid attributes' do
    expect(@make).to be_valid
  end

  it 'has a unique name' do
    make2 = build(:make, name: @make.name)
    expect(make2).to_not be_valid
  end

  it 'is not valid without a name' do
    @make.name = nil
    expect(@make).to_not be_valid
  end
end
