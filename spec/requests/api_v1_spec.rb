require 'rails_helper'

RSpec.describe V1::Offers, type: :request do
  after(:each) do
    Faker::Vehicle.unique.clear
  end

  context 'GET /api/v1' do
    it 'returns an empty array of offers' do
      get '/api/v1/offers'
      expect(response.status).to eq(200)
      expect(JSON.parse(response.body)).to eq 'offers' => []
    end

    it 'returns an array with one offer' do
      create(:offer)
      get '/api/v1/offers'
      expect(response.status).to eq(200)
      data = Entities::OfferDetailedEntity.represent(Offer.ordered).to_json
      expect(response.body).to eq data
    end
  end

  context 'GET /api/v1/offers/:id' do
    it 'returns an offer by id' do
      offer = create(:offer)
      get "/api/v1/offers/#{offer.id}"
      data = Entities::OfferDetailedEntity.represent(offer).to_json
      expect(response.body).to eq data
    end

    it 'returns 404 error for non-existing offer' do
      get '/api/v1/offers/1'
      expect(response.status).to eq 404
    end
  end

  context 'POST /api/v1/offers' do
    it 'create new offer with valid params' do
      make = create(:make)
      model = create(:model)
      user = create(:user)

      token = AuthTokens::EncodeFrontendTokenInteraction.run!(user: user)
      valid_params = {
        offer: FactoryBot.attributes_for(:offer).merge(make_id: make.id, model_id: model.id),
        token: token
      }
      expect(Offer.count).to eq 0

      post '/api/v1/offers', params: valid_params

      expect(response.status).to eq 201
      expect(Offer.count).to eq 1
    end

    it 'returns 401 error with valid token' do
      make = create(:make)
      model = create(:model)
      token = Faker::String.random(12)
      invalid_params = {
        offer: FactoryBot.attributes_for(:offer).merge(make_id: make.id, model_id: model.id),
        token: token
      }
      post '/api/v1/offers', params: invalid_params
      expect(response.status).to eq 401
    end
  end
end
