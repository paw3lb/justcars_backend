FactoryBot.define do
  car_options = Faker::Vehicle.car_options.join(', ')
  standard_specs = Faker::Vehicle.standard_specs.join(', ')

  factory :offer do
    title { Faker::Vehicle.make_and_model }
    description { "Car options: #{car_options} Standard specs: #{standard_specs}" }
    price { BigDecimal(100_000) }
    make
    model
    image { [fixture_file_upload(Rails.root.join('spec', 'support', 'assets', 'car1.jpg'), 'image/jpg')] }
  end
end
