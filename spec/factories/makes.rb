FactoryBot.define do
  factory :make do
    name { Faker::Vehicle.unique.make }
  end
end
