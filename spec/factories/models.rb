FactoryBot.define do
  factory :model do
    name { Faker::Vehicle.unique.model }
    make
  end
end
