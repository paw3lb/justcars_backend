# frozen_string_literal: true

# This file is used by Rack-based servers to start the application.

require_relative 'config/environment'

run Rails.application

use Rack::Cors do
  allow do
    origins '*'
    # location of your API
    resource '*', headers: :any, methods: %i[get post put patch delete]
  end
end
