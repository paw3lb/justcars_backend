class OfferDecorator < ApplicationDecorator
  include Rails.application.routes.url_helpers

  def image_url
    rails_blob_url(object.image, Rails.application.config.action_mailer.default_url_options) if object.image.attached?
  end

  def make_name
    object.make.name
  end

  def model_name
    object.model.name
  end
end
