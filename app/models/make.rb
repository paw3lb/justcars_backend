class Make < ApplicationRecord
  has_many :models
  has_many :offers

  validates :name, presence: true, uniqueness: true
end
