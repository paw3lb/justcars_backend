class Model < ApplicationRecord
  belongs_to :make
  has_many :offers

  validates :name, presence: true, uniqueness: true
  validates :make_id, presence: true
end
