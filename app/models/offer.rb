class Offer < ApplicationRecord
  belongs_to :make
  belongs_to :model
  has_one_attached :image

  # delegate :name, to: :make, prefix: true, allow_nil: true
  # delegate :name, to: :model, prefix: true, allow_nil: true
  scope :ordered, -> { order('id DESC') }

  validates :title, :description, :price, :make_id, :model_id, presence: true
  validates :title, length: { maximum: 255 }
  validates :description, length: { maximum: 3_000 }
  validates :price, format: { with: /\A\d+(?:\.\d{0,2})?\z/ }, numericality: { greater_than: 0, less_than: 100_000_000 }
end
