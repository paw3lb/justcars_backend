class BaseAPI < Grape::API
  helpers do
    def authenticate!
      error!('401 Unauthorized', 401) unless valid_user?
    end

    def clean_params(params)
      ActionController::Parameters.new(params)
    end

    def current_user
      token = params[:token] || request.headers['Authorization']
      @current_user ||= Users::FindCurrentUserInteraction.run!(token: token)
    end

    def valid_user?
      current_user.present?
    end
  end

  route :any, '*path' do
    error!({ status: :error, message: :not_found }, 404)
  end
end
