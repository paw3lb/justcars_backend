module V1
  class Offers < BaseAPI
    rescue_from ActiveRecord::RecordNotFound do |_e|
      error!({ status: :error, message: :not_found }, 404)
    end
    rescue_from :all

    version 'v1', using: :path, vendor: 'justcars'
    format :json
    prefix :api

    before do
      header 'Access-Control-Allow-Origin', '*'
    end

    resource :offers do
      desc 'Return all offers'
      get '' do
        present OffersInteractions::FindOffersInteraction.run!(offers_params: params[:q]), with: Entities::OfferDetailedEntity
      end

      desc 'Get an offer details'
      params do
        requires :id, type: String, desc: 'ID of the offer'
      end
      get ':id' do
        present Offer.find(params[:id]), with: Entities::OfferDetailedEntity
      end

      desc 'Create an offer'
      params do
        requires :token, type: String, desc: 'Auth token'
        requires :offer, type: Hash, desc: 'Offer object to create' do
          requires :title, type: String, desc: 'Offer title'
          requires :description, type: String, desc: 'Description of offer'
          requires :price, type: BigDecimal, desc: 'Price'
          requires :make_id, type: Integer, desc: 'Make ID'
          requires :model_id, type: Integer, desc: 'Model ID'
          optional :image, type: [Rack::Test::UploadedFile, Rack::Multipart::UploadedFile], desc: 'Image file'
        end
      end
      post do
        authenticate!
        safe_params = clean_params(params[:offer]).permit(:title, :description, :price, :make_id, :model_id, :image)

        if safe_params
          Offer.create!(safe_params)
          status 201
        end
      end
    end
  end
end
