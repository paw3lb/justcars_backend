module Entities
  class OfferEntity < Grape::Entity
    root :offers, :offer
    expose :id
  end
end
