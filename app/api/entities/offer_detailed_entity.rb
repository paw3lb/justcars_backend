module Entities
  class OfferDetailedEntity < OfferEntity
    expose :title
    expose :description
    expose :price
    expose :make_name
    expose :model_name
    expose :image_url, as: :imageUrl

    def image_url
      object.decorate.image_url
    end

    def make_name
      object.decorate.make_name
    end

    def model_name
      object.decorate.model_name
    end
  end
end
