module Users
  class FindCurrentUserInteraction < ActiveInteraction::Base
    boolean :simple_auth, default: false
    string :token

    validates :token, presence: true

    def execute
      data = AuthTokens::DecodeFrontendTokenInteraction.run!(token: token)

      return true if simple_auth.present?

      User.find_or_initialize_by(id: data['user_id'])
    rescue StandardError
      nil
    end
  end
end
