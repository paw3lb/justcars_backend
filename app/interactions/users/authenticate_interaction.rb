module Users
  class AuthenticateInteraction < ActiveInteraction::Base
    string :email
    string :password

    validates :email, presence: true
    validates :password, presence: true

    def execute
      user = User.find_or_initialize_by(email: email)

      if user.persisted? && user.valid_password?(password)
        user.jwt_token = AuthTokens::EncodeFrontendTokenInteraction.run!(user: user)
      end

      user
    end
  end
end
