module OffersInteractions
  class FindOffersInteraction < ActiveInteraction::Base
    hash :offers_params, default: {} do
      decimal :price_gteq, default: nil
      decimal :price_lteq, default: nil
      string :title_cont, default: nil
    end

    validates :offers_params, presence: true

    def execute
      Offer.includes(:make, :model).with_attached_image.ordered.ransack(offers_params).result
    end
  end
end
