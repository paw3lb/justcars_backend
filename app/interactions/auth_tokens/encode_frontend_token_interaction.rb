module AuthTokens
  class EncodeFrontendTokenInteraction < ActiveInteraction::Base
    attr_reader :config

    object :user, class: User

    validates :user, presence: true

    def execute
      @config = AuthTokens::FrontendTokenConfigInteraction.run.result

      JWT.encode payload, config.hmac_secret, config.algorithm
    end

    private

    def payload
      {
        user_id: user.id,
        iss: config.issuer,
        iat: config.issued_at,
        exp: config.expiration
      }
    end
  end
end
