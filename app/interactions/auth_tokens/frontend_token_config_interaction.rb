module AuthTokens
  class FrontendTokenConfigInteraction < ActiveInteraction::Base
    ALGORITHM = 'HS512'.freeze
    ISSUER = 'justcars'.freeze
    HMAC_SECRET = Rails.application.credentials.jwt_secret.freeze

    def execute
      OpenStruct.new(
        algorithm: ALGORITHM,
        issuer: ISSUER,
        expiration: expiration,
        issued_at: issued_at,
        hmac_secret: HMAC_SECRET
      )
    end

    private

    def expiration
      Time.current.to_i + 12 * 3600
    end

    def issued_at
      Time.current.to_i
    end
  end
end
