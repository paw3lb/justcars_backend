module AuthTokens
  class DecodeFrontendTokenInteraction < ActiveInteraction::Base
    attr_reader :config

    string :token

    validates :token, presence: true

    def execute
      @config = AuthTokens::FrontendTokenConfigInteraction.run.result

      data = JWT.decode token, config.hmac_secret, true, algorithm: config.algorithm, iss: config.issuer, verify_iss: true,
                                                         verify_iat: true

      data.first
    end
  end
end
